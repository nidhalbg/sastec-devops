# Sastec Devops

# Available Images
The repo includes the following containers:

- nginx (latest)
- mySQL (5.7)
- phpMyAdmin
- Redis (latest)
- PHP 7
- Mailhog

## How do I use it?
1. clone the repo
```
git clone https://gitlab.com/nidhalbg/sastec-devops.git
```
2. cd appN
3. Run 
```
$ docker-compose up -d
```